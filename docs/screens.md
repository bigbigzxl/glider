# Eink Screens

This is a list of Eink screens and their key parameters and their compatibilities with Caster/ Glider. The information are gathered from public sources, so they might be incorrect. This is not a complete list of all screens Eink have ever produced or in production. This table is intended for hobbiests buying used screens. If you are designing a product with Eink screen please contact Eink directly.

Other than a few exceptions, only screens without integrated TCON are listed here (in other words, SPI screens are generally not included here). These screens are the main focus of this project anyway.

Screen size is the first 3 numbers in the model number, so it's not listed separately in the table. For example, ED060SC4 is 6.0", ED097OC1 is 9.7", and ES133UT1 is 13.3".

The adapter column refers to the adapter needed for this particular screen, however there is no guarentee that it would work, even if it's listed as tested.

| Model Name | Model Number | FPL Platform | Resolution  | Marketing Name              | R Typ | CR Typ | Year  | Interface | Pin Count | Adapter | Tested? |
| ---------- | ------------ | ------------ | ----------- | --------------------------- | ----- | ------ | ----- | --------- | --------- | ------- | ------- |
| ED043WC1   |              | V220         | 800x480     | Pearl                       | 35%   | 12:1   | 2013  | TTL       | 39        | 39P-C   |         |
| ED043WC3   | VA3200-DCA   | V220         | 800x480     | Pearl                       |       |        | 2014  | TTL       | 39        | 39P-C   |         |
| ED043WC5   | VD1405-CGA   | 400          | 800x480     | Carta 1200                  |       |        |       | SPI       |           |         |         |
| ED047TC1   |              | V220         | 960x540     | Pearl                       | 35%   | 12:1   | 2015  | TTL       | 44        |         |         |
| ED047TC2   |              | V220         | 960x540     | Pearl                       | 35%   | 12:1   | 2016  | TTL       | 44        |         |         |
| ET047TC1   |              | 320          | 960x540     | Carta 1.2                   |       |        |       | TTL       |           |         |         |
| ED050SC3   |              | V110         | 800x600     | Vizplex                     | 35%   | \>6:1  | 2008  | TTL       | 33        |         |         |
| ED050SU3   |              | V220         | 800x600     | Pearl                       |       |        |       | TTL       |           |         |         |
| ED052TC2   |              | 320          |             | Carta                       | 45%   | 16:1   | 2016  | TTL       | 40        |         |         |
| ED052TC4   | VB3300-EBA   | 320          | 1280x720    | Carta 1.2                   | 45%   | 16:1   | 2017  | TTL       | 50        |         |         |
| EC058TC1   | SA1452-EHA   | 320          | 1440x720    | Kaleido / Carta             | 24%   | 15:1   | 2020  | TTL       | 50        |         |         |
| ED058TC7   |              | 320          |             | Carta                       |       |        |       | TTL       |           |         |         |
| ED058TC8   | VB3300-EHB   | 320          | 1440x720    | Carta                       |       |        |       | TTL       |           |         |         |
| ED060SC4   |              | V110         | 800x600     | Vizplex                     | 35%   | \>6:1  | 2008  | TTL       | 39        |         |         |
| ED060SC7   |              | V220E        | 800x600     | Pearl                       | 40%   | 12:1   | 2010  | TTL       |           |         |         |
| ED060SCA   |              | V110         | 800x600     | Vizplex                     |       |        |       | TTL       |           |         |         |
| ED060SCE   |              | V220/V220E   | 800x600     | Pearl                       |       |        |       | TTL       |           |         |         |
| ED060SCF   |              | V220         | 800x600     | Pearl                       |       |        |       | TTL       |           |         |         |
| ED060SCG   |              | V220E        | 800x600     | Pearl                       |       |        |       | TTL       |           |         |         |
| ED060SCN   |              | V220E        | 800x600     | Pearl                       |       |        |       | TTL       |           |         |         |
| ED060SCP   |              | V220         | 800x600     | Pearl                       |       |        |       | TTL       |           |         |         |
| ED060SCQ   |              | V220         | 800x600     | Pearl                       |       |        |       | TTL       |           |         |         |
| ED060SCS   |              |              | 800x600     |                             |       |        |       | TTL       |           |         |         |
| ED060SCT   |              | 320          | 800x600     | Carta                       |       |        |       | TTL       |           |         |         |
| ED060SD1   |              | 320          | 800x600     | Carta                       |       |        |       | TTL       |           |         |         |
| ED060XC3   |              | V220         | 1024x758    | Pearl                       |       |        |       | TTL       | 34        | 34P-A   | Yes     |
| ED060XC5   |              | V220         | 1024x758    | Pearl                       | 35%   | 12:1   | 2011  | TTL       |           |         |         |
| ED060XC8   |              | V320         | 1024x758    | Carta                       |       |        |       | TTL       | 35        | 35P-A   | Yes     |
| ED060XCD   |              | 320          | 1024x758    | Carta                       |       |        |       | TTL       |           |         |         |
| ED060XCG   | VD1405-FOA   | 320/400      | 1024x758    | Carta 1000 / 1200           | 40%   | 17:1   | 2020  | TTL       |           |         |         |
| ED060XCH   | VD1405-FOE   | 400          | 1024x758    | Carta 1200                  |       |        |       | TTL       |           |         |         |
| ED060XD4   |              | 320          | 1024x758    | Carta                       |       |        |       | TTL       | 34        | 34P-A   | Yes     |
| ED060XG1   |              | V110/V220    | 1024x758    | Vizplex / Pearl             | 40%   | 12:1   | 2012  | TTL       |           |         |         |
| ED060XG2   |              | V220         | 1024x758    | Pearl                       |       |        |       | TTL       |           |         |         |
| ED060XG3   |              | 320          | 1024x758    | Carta                       |       |        |       | TTL       |           |         |         |
| ED060XH7   |              | 320          | 1024x758    | Carta 1.2                   | 45%   | 17:1   | 2015  | TTL       |           |         |         |
| ED060XH9   | VB3300-FOG   | 320          | 1024x758    | Carta                       |       |        |       | TTL       |           |         |         |
| ED060TC1   |              | 320          | 1448x1072   | Carta                       |       |        |       | TTL       | 35        | 35P-A   |         |
| ED060KC1   |              | 320          | 1448x1072   | Carta                       | 46%   | 17:1   | 2014  | TTL       | 34        | 34P-A   |         |
| ED060KC4   |              | 320          | 1448x1072   | Carta                       |       |        |       | TTL       |           |         |         |
| ED060KD1   |              | 320          | 1448x1072   | Carta                       |       |        |       | TTL       | 34        | 34P-A   | Yes     |
| ED060KG1   |              | 320          | 1448x1072   | Carta                       | 47%   | 17:1   | 2015  | TTL       | 34        | 34P-A   |         |
| ED060KH4   |              | 320          | 1448x1072   | Carta                       |       |        |       | TTL       |           |         |         |
| ED060KH6   | VB3300-FOE   | 320          | 1448x1072   | Carta                       |       |        |       | TTL       |           |         |         |
| ED060KHC   |              |              | 1448x1072   |                             |       |        |       | TTL       |           |         |         |
| EC060KH3   | SA1452-FOA   |              | 1448x1072   | Kaleido                     |       |        |       | TTL       |           |         |         |
| ED061KC1   | VD1405-FAA   | 400          | 1648x824    | Carta 1200                  |       |        |       | TTL       |           |         |         |
| ED067KC1   | VB3300-FGA   | 320          | 1800x900    | Carta                       | 45%   | 16:1   | 2020  | TTL       | 50        | 50P-B   |         |
| EC067KC1   | SA1452-FGA   |              | 1800x900    | Kaleido                     |       |        |       | TTL       | 50        | 50P-B   |         |
| ED068TG1   |              | 320          | 1440x1080   | Carta                       |       |        | <2013 | TTL       |           |         |         |
| ED068TH1   |              | 320          | 1440x1080   | Carta                       |       |        | <2014 | TTL       |           |         |         |
| ED068TH3   | VB3300-FHA   | 320          | 1440x1080   | Carta                       |       |        |       | TTL       |           |         |         |
| ED068KC1   |              | 400SU        | 1648x1236   | Carta 1200                  |       |        |       | TTL       |           |         |         |
| ED068KC5   | VD1405-FHF   | 400          | 1648x1236   | Carta 1200                  | \>44% | \>19:1 |       | TTL       |           |         |         |
| ED070KC2   |              | 320          | 1680x1264   | Carta 1100                  | \>47% | \>16:1 |       | TTL       |           |         |         |
| ED070KC3   |              | 320          | 1680x1264   | Carta 1100                  |       |        |       | TTL       |           |         |         |
| ED070KC4   | VD1400-GOC   | 400          | 1680x1264   | Carta 1200                  |       |        |       | TTL       |           |         |         |
| ED070KH1   |              | 320          | 1680x1264   | Carta 1100                  |       |        |       | TTL       |           |         |         |
| EC070KH1   | SC1452-GOA   |              | 1680x1264   | Kaleido Plus                |       |        |       | TTL       |           |         |         |
| LB071WS1   |              |              | 1024x600    |                             |       | 7:1    |       | TTL       |           |         |         |
| ET073TC1   |              | V320         | 750x200     | Carta                       |       |        | 2016  | TTL       |           |         |         |
| ED078KC1   |              |              | 1872x1404   | Carta 1.2                   | 45%   | 16:1   | 2016  | TTL       |           |         |         |
| ED078KC2   | VB3300-GHC   | 320          | 1872x1404   | Carta                       |       |        |       | TTL       |           |         |         |
| ED078KH1   |              | 320          | 1872x1404   | Carta                       |       |        |       | TTL       |           |         |         |
| ED078KH3   |              | 320          | 1872x1404   | Carta 1.2                   |       |        |       | TTL       |           |         |         |
| ED078KH4   | VB3300-GHB   | 320          | 1872x1404   | Carta                       |       |        |       | TTL       |           |         |         |
| EC078KH3   | SC1452-GHA   |              | 1872x1404   | Kaleido Plus                |       |        |       | TTL       |           |         |         |
| EC078KH4   | SC1452-GHB   |              | 1872x1404   | Kaleido Plus ?              |       |        |       | TTL       |           |         |         |
| EC078KH5   | SC1452-GHC   |              | 1872x1404   | Kaleido Plus ?              |       |        |       | TTL       |           |         |         |
| EC078KH6   | SC1452-GHD   |              | 1872x1404   | Kaleido 3                   |       |        |       | TTL       |           |         |         |
| EC078KH7   | SC1452-GHE   |              | 1872x1404   | Kaleido 3                   |       |        |       | TTL       |           |         |         |
| ED080XC1   |              | V110         | 1024x768    | Vizplex                     |       |        |       | TTL       |           |         |         |
| ED080TC1   |              | V220         | 1600x1200   | Pearl                       |       |        |       | TTL       |           |         |         |
| EC080SC2   |              | V250         | 600xRGBx800 | Triton 2                    |       |        |       | TTL       |           |         |         |
| ES080KC2   | VD1400-HOB   | 400          | 1920x1440   | Carta 1200                  |       |        |       | TTL       |           |         |         |
| ES080KH1   |              |              |             |                             |       |        |       |           |           |         |         |
| AC080KH1   | AD1004-HOA   | HAL3         | 1920x1440   | Gallery 3                   |       |        |       | MiniLVDS  |           |         |         |
| ED097OC1   |              | V110A        | 1200x825    | Vizplex                     | 35%   | 7:1    | 2008  | TTL       |           |         |         |
| ED097OC4   |              | V110A/V220   | 1200x825    | Vizplex / Pearl             |       |        |       | TTL       |           |         |         |
| ED097OD2   |              | V220         | 1200x825    | Pearl                       |       |        |       | TTL       |           |         |         |
| ED097TC1   |              | V220         | 1200x825    | Pearl                       |       |        |       | TTL       |           |         |         |
| ED097TC2   | VB3300-JGA   | 320          | 1200x825    | Carta 1.2                   | 42%   | 16:1   | 2016  | TTL       |           |         |         |
| EL097TR2   | EA2220-JGB   |              | 1200x825    | Spectra 3000                |       |        |       | TTL       |           |         |         |
| ED100UC1   | VB3300-KOA   | 320          | 1600x1200   | Carta                       | 45%   | 16:1   | 2020  | TTL       | 40        | DIRECT  |         |
| ES103TC1   | VB3300-KCA   | 320          | 1872x1404   | Carta 1.2                   | 40%   | 12:1   | 2016  | TTL       | 40        | DIRECT  |         |
| ED103TC2   | VB3300-KCD   | 320          | 1872x1404   | Carta                       | 43%   | 14:1   | 2019  | TTL       | 40        | DIRECT  |         |
| ES103TD1   |              | 320          | 1872x1404   | Carta                       |       |        |       | TTL       |           |         |         |
| ES103TD3   |              | 320          | 1872x1404   | Carta                       |       |        |       | TTL       |           |         |         |
| EC103TD1   | SA1452-KCC   |              | 1872x1404   | Kaleido                     |       |        |       | TTL       |           |         |         |
| EC103TH2   | SC1452-KCB   |              | 1872x1404   | Kaleido Plus                |       |        |       | TTL       |           |         |         |
| EC103KH2   | SC1452-KCD   |              | 2480x1860   | Kaleido 3                   |       |        |       | TTL       |           |         |         |
| ES107KC1   | VD1400-KGA   | 400          | 2560x1920   | Carta 1200                  |       |        |       | TTL       |           |         |         |
| ES108FC1   |              | 320          |             | Carta                       | 46%   | 16:1   | 2017  | TTL       | 50        |         |         |
| ED113TC1   | VB3300-LCA   | 320          | 2400x1034   | Carta                       | 35%   | 12:1   | 2017  | TTL       | 50        | 50P-A   |         |
| ED113TC2   | VB3300-LCB   | 320          | 2400x1034   | Carta 1.2                   | 35%   | 12:1   | 2019  | TTL       | 50        | 50P-A   |         |
| EC113TC1   | SC1452-LCA   |              | 2400x1034   | Kaleido Plus ?              |       |        |       | TTL       | 50        | 50P-A   |         |
| ED115OC1   |              | V220         | 2760x2070   | Pearl                       | 35%   | 12:1   | 2012  | TTL       | 40        | DIRECT  |         |
| ES120MC1   | VD1400-MOA   | 400          | 2560x1600   | Carta 1200                  |       |        |       | TTL       | 40        |         |         |
| ES133UT1   |              | V220         | 1600x1200   | Pearl                       | 35%   | 12:1   | 2013  | TTL       | 39        | 39P-A   | Yes     |
| ES133UT2   |              | 320          | 1600x1200   | Carta                       |       |        |       | TTL       | 39        | 39P-A   | Yes     |
| ES133UE2   |              | 320          | 1600x1200   | Carta                       |       |        |       | TTL       | 39        | 39P-A   |         |
| ED133UT2   | VB3300-NCB   | 320          | 1600x1200   | Carta 1.2                   | 45%   | 16:1   | 2016  | TTL       | 39        | 39P-A   |         |
| ED133UT3   | VB3300-NCC   | 320          | 1600x1200   | Carta                       | 45%   | 16:1   | 2019  | TTL       | 39        | 39P-A   |         |
| ES133TT3   |              | 320          | 2200x1650   | Carta 1.2                   | 40%   | 12:1   | 2016  | TTL       | 39        | 39P-B   |         |
| ES133TT5   | VH1948-NCC   | 450          | 2200x1650   | Carta 1250                  |       |        |       | TTL       | 39        | 39P-B   |         |
| EC133UJ1   | SD1452-NCB   |              | 1600x1200   | Kaleido 3 Outdoor           |       |        |       | TTL       | 39        | 39P-A   |         |
| AC133UT1   | AA1020-NCA   |              | 1600x1200   | Gallery / Gallery 4000      | 35%   | 10:1   | 2020  | TTL       | 39        | 39P-A   |         |
| EL133US1   |              |              | 1600x1200   | Spectra 3000                |       |        |       | TTL       | 39        | 39P-A   | Yes     |
| EL133UR1   | EA2220-NCC   |              | 1600x1200   | Spectra 3000                | 33%   | 15:1   | 2020  | TTL       | 39        | 39P-A   |         |
| EL133UF1   | ED2208-NCA   |              | 1600x1200   | Spectra 6                   |       |        |       | QSPI      |           |         |         |
| ED140TT1   | VB3300-IDA   | 320          | 1440x300    | Carta                       |       |        |       | TTL       |           |         |         |
| AC253TT1   | AA1020-PEA   |              | 3200x1800   | Gallery Plus / Gallery 4000 | 35%   | 10:1   | 2020  | MiniLVDS  | 51x2      |         |         |
| EL253EW1   | ED2208-PEA   |              | 3200x1800   | Spectra 6                   |       |        |       | MiniLVDS  |           |         |         |
| EC253TT1   | SD1452-PEA   |              | 3200x1800   | Kaleido 3 Outdoor           |       |        |       | MiniLVDS  |           |         |         |
| ED253TT1   | VB3300-PEA   | 320          | 3200x1800   | Carta 1.2                   |       |        |       | MiniLVDS  | 51x2      |         |         |
| ED253TT2   | VB3300-PEB   | 320          | 3200x1800   | Carta 1.2                   |       |        |       | MiniLVDS  | 51x2      |         |         |
| ED253TT3   | VB3300-PEC   | 320          | 3200x1800   | Carta 1.2                   |       |        |       | MiniLVDS  | 51x2      |         |         |
| EL253TV1   | EB2200-PEA   |              | 3200x1800   | Spectra 3100                |       |        |       | MiniLVDS  | 51x2      |         |         |
| ED280TT1   | VB3300-PHA   | 320          | 3840x1080   | Carta 1.2                   | 40%   | 12:1   | 2020  | MiniLVDS  | 51x2      |         |         |
| ED312TT2   | VA3200-QAA   | V220         | 2560x1440   | Pearl                       |       |        |       | TTL       | 50x4      |         |         |
| ED312TT3   | VA3200-QAB   | V220         | 2560x1440   | Pearl                       | 40%   | 12:1   | 2018  | TTL       | 50x4      |         |         |
| EC312TT2   | SB1452-QAA   |              | 2560x1440   | Triton                      |       |        |       | TTL       | 50x4      |         |         |
| EL315TW1   | ED2208-QBA   |              | 2560x1440   | Spectra 6                   |       |        |       | QSPI      |           |         |         |
| ED420TT1   |              | V220         | 2880x2160   | Pearl                       |       |        |       | TTL       | 50x2      |         |         |
| ED420TT3   | VB3300-RBA   | 320          | 2880x2160   | Carta 1.2                   | 45%   | 16:1   | 2020  | TTL       | 50x2      |         |         |
| ED420TT5   | VB3300-RBB   | 320          | 2880x2160   | Carta 1.2                   |       |        |       | TTL       | 50x2      |         |         |

Note: Carta 1.2 is also known as Carta 1000. If the table cell says Carta it also likely means Carta 1000 (could be 1100 as well, I don't know for sure).
